const limitFunctionCallCount = require("../limitFunctionCallCount.cjs");

function cb(name) {
  return `Hi ${name}`;
}
let result = limitFunctionCallCount(cb, 2);
console.log(result());
console.log(result("sulochana"));
console.log(result("hello"));
console.log(result("everyone"));
