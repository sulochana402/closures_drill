const cacheFunction = require("../cacheFunction.cjs");

function cb(...args) {
  return args;
}
let result = cacheFunction(cb);
console.log(result(1, 2, 3, 4, 5));
console.log(result(6, 7, 8));
console.log(result(1, 2, 3));
