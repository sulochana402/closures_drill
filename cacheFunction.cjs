function cacheFunction(cb) {
  const cache = {};
  return function (...args) {
    const argsString = JSON.stringify(args);

    if (cache[argsString] !== undefined) {
      return cache[argsString];
    } else {
      const result = cb(...args);
      cache[argsString] = result;
      return result;
    }
  };
}
module.exports = cacheFunction;
